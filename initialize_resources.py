import sqlite3
import logging
from collections import OrderedDict
# map table names to SQL queries
table_definitions = OrderedDict()
table_definitions.update({
    "users": """CREATE TABLE users
            (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                username TEXT NOT NULL,
                password TEXT NOT NULL
            )"""
})

table_definitions.update({
    "events": """CREATE TABLE events
            (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                owner INTEGER NOT NULL,
                name TEXT,
                description TEXT,
                FOREIGN KEY(owner) REFERENCES users(id)
            )"""

})

table_definitions.update({
    "photos": """CREATE TABLE photos
            (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                owner INTEGER NOT NULL,
                event INTEGER,
                date TEXT,
                path TEXT,
                FOREIGN KEY(owner) REFERENCES users(id)
            )
            """
})


def main():
    logging.basicConfig(level=logging.INFO)
    connection = sqlite3.connect("database.db")
    c = connection.cursor()
    for name, query in table_definitions.items():
        try:
            c.execute(query)
            logging.info("%s table initialized.", name)
        except sqlite3.OperationalError as e:
            if "already exists" in str(e):
                pass
            else:
                print(query)
                raise e
    connection.commit()
    connection.close()

if __name__ == '__main__':
    main()
