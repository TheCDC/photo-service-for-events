import db
import flask_login

class AuthenticationError(Exception):
    pass

class User(flask_login.UserMixin):
    """Construct a uer object from  db only by username and password."""

    def __init__(self, username, password):
        super().__init__()
        with db.connect() as (_, c):
            c.execute("""SELECT * FROM users
                WHERE username = ? AND password = ?""",
                      (username, password))
            results = c.fetchall()
            if len(results) == 0:
                raise AuthenticationError("User does not exist!")
        record = results[0]
        self.id = str(record[0])
        self.name = record[1]
        # self.is_active = True
        # self.is_authenticated = True
        # self.is_anonymous = False

    def get_id(self):
        return self.id


def from_id(unicode_id):
    # refer to a user only by record id
    with db.connect() as (_, c):
        # select the user with matching id
        c.execute("""SELECT * FROM users WHERE id = ?
            """,
                  (unicode_id,))
        results = c.fetchall()
    row = results[0]
    # reconstitute the user from db record
    user = User(row[1], row[2])
    return user


def register(username, password):
    """Create a new user in db by username and password."""
    with db.connect() as (_, c):
        # find mathing users by username
        c.execute("""SELECT * FROM users
            WHERE username = ?""",
                  (username,))
        results = c.fetchall()
    # there should only be 1 or 0 but just check for any > 0
    if len(results) > 0:
        raise AuthenticationError("username already exists")
    else:
        # when no conflicts with existing users, create new db record
        with db.connect() as (_,c):
            c.execute("INSERT INTO users VALUES (NULL,?,?)",
                      (username, password))
        new_user = User(username, password)
        return new_user
