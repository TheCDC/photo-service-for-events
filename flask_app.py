#!/usr/bin/python3


import flask
import flask_login
import initialize_resources
from werkzeug.utils import secure_filename
import config
import db
import users
import logging
initialize_resources.main()
app = flask.Flask(__name__)
app.secret_key = "OMG CHANGE THIS IMMEDIATELY"
logging.warning("WARNING secret key not set! Application is insecure!")
# site-wide variables
app.config["UPLOAD_FOLDER"] = config.UPLOAD_FOLDER
app.config["DB_PATH"] = config.DB_PATH


# instantiate and initialize login manager
login_manager = flask_login.LoginManager()
login_manager.init_app(app)


@login_manager.user_loader
def load_user(user_id):
    return users.from_id(user_id)


@app.route("/")
def home():
    return flask.render_template("home.html")


@app.route("/login", methods=["GET", "POST"])
def login():
    if flask.request.method == "GET":
        return flask.render_template("login.html")
    else:
        username = flask.request.form["username"]
        password = flask.request.form["password"]
        try:
            # attempt to reconstitute the user from the db
            cur_user = users.User(username, password)
            # magic that sets the session for the logged in user
            flask_login.login_user(cur_user)
        except users.AuthenticationError:
            # handle inability to login for various reasons
            return flask.redirect(flask.url_for("bad", message="Invalid login"))
        return flask.redirect(flask.url_for("home"))


@app.route("/register", methods=["GET", "POST"])
def register():
    if flask.request.method == "GET":
        return flask.render_template("register.html")
    else:
        username = flask.request.form["username"]
        password = flask.request.form["password"]
        try:
            new_user = users.register(username, password)
            flask_login.login_user(new_user)
            return flask.render_template("home.html")
        except users.AuthenticationError as e:
            # exception when user already exists
            return flask.redirect(flask.url_for("bad",
                                                message="User already exists"))


@app.route("/logout")
def logout():
    flask_login.logout_user()
    return flask.redirect(flask.url_for("home"))


@app.route("/bad")
def bad():
    message = flask.request.args.get("message")
    return flask.render_template("bad.html", message=message)
if __name__ == '__main__':
    app.run()
