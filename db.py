import contextlib
import sqlite3
import config

@contextlib.contextmanager
def connect():
    """A simple context manager to have a short way
    of connecting to the DB and automatically commiting changes
    and closing the connection."""
    connection = sqlite3.connect(config.DB_PATH)
    c = connection.cursor()
    yield connection, c
    connection.commit()
    connection.close()
